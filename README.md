[![pipeline status](https://gitlab.fel.cvut.cz/hamsatom/RxJava-NonBlocking-HTTP-Server/badges/master/pipeline.svg)](https://gitlab.fel.cvut.cz/hamsatom/RxJava-NonBlocking-HTTP-Server/commits/master)

**Autor:** Tomáš Hamsa  
**Javadoc:** http://hamsatom.pages.fel.cvut.cz/RxJava-NonBlocking-HTTP-Server/

Používání serveru probíhá tak, že spustíte main a pak například z terminálu se na něj připojíte. První argument je port serveru a druhý root složka, což je nejvyšší složka z které může server vracet soubory.  
Server má v rootu .htaccess soubor který omezuje přístup k souborům. Defaultní username je test a heslo 123456
Z terminálu se jde připojit například tak, že použijete telnet, takže telnet localhost (port serveru).  
A pak pošlete http request jako  
GET /dataFolder/picture.jpg HTTP/1.0  
Authorization: Basic dGVzdDoxMjM0NTY=  
a nazpátek dostanete http response se souborem binárně.  
Pokud člověk smaže .htaccess v rootu nebo donutí prohlížeč poslat basic authentifikaci, je  možno spustit server a připojit se na něj z prohlížeš, například na http://localhost:8080/dataFolder/picture.jpg  
