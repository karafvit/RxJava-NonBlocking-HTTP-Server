package ass.hamsatom.semestral.filter;

import ass.hamsatom.semestral.Creator;
import ass.hamsatom.semestral.constat.HttpCode;
import ass.hamsatom.semestral.response.ErrorResponse;
import ass.hamsatom.semestral.response.Response;
import io.reactivex.Observable;
import org.testng.annotations.Test;

/**
 * @author Tomáš Hamsa on 12.05.2017.
 */
public class RequestFilterImplTest {

  @Test
  public void testFilterResponses() {
    RequestFilter filter = Creator.getFilter();
    Creator.createObservableRequest(Creator.TEST_FILE, Creator.PASSWORD, Creator.USERNAME)
        .compose(filter)
        .map(Response::isValid)
        .test()
        .assertComplete()
        .assertNoErrors()
        .assertValue(true);
  }

  @Test
  public void testFilterInvalidResponses() {
    RequestFilter filter = Creator.getFilter();
    Observable.just(Creator.createInvalidRequest()).compose(filter)
        .map(response -> !response.isValid() && response instanceof ErrorResponse
            && response.getCode() == HttpCode.NOT_FOUND)
        .test()
        .assertComplete()
        .assertNoErrors()
        .assertValue(true);
  }

  @Test
  public void testFilterResponsesWithPermission() {
    RequestFilter filter = Creator.getFilter();
    Creator.createObservableRequest(Creator.PROTECTED_FILE, Creator.PASSWORD, Creator.USERNAME)
        .compose(filter)
        .map(Response::isValid)
        .test()
        .assertComplete()
        .assertNoErrors()
        .assertValue(true);
  }

  @Test
  public void testFilterResponsesWithoutPermission() {
    RequestFilter filter = Creator.getFilter();
    Creator.createObservableRequest(Creator.PROTECTED_FILE, "", Creator.USERNAME).compose(filter)
        .map(response -> !response.isValid() && response instanceof ErrorResponse
            && response.getCode() == HttpCode.UNAUTHORIZED)
        .test()
        .assertComplete()
        .assertNoErrors()
        .assertValue(true);
  }

  @Test
  public void testFilterResponsesAboveRoot() {
    RequestFilter filter = Creator.getFilter();
    Creator.createObservableRequest("../" + Creator.TEST_FILE, "", "").compose(filter)
        .map(response -> !response.isValid() && response instanceof ErrorResponse
            && response.getCode() == HttpCode.NOT_FOUND)
        .test()
        .assertComplete()
        .assertNoErrors()
        .assertValue(true);
  }
}