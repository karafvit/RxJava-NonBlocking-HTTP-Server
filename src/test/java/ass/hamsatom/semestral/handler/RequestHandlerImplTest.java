package ass.hamsatom.semestral.handler;

import ass.hamsatom.semestral.Creator;
import ass.hamsatom.semestral.constat.HttpCode;
import ass.hamsatom.semestral.request.ParsedRequest;
import ass.hamsatom.semestral.response.ErrorResponse;
import ass.hamsatom.semestral.response.Response;
import io.reactivex.Observable;
import java.util.Optional;
import org.testng.annotations.Test;

/**
 * @author Tomáš Hamsa on 12.05.2017.
 */
public class RequestHandlerImplTest {

  @Test
  public void testHandleValidRequest() {
    RequestHandler handler = Creator.getRequestHandler();
    Creator.createObservableRequest(Creator.TEST_FILE, "", "").compose(handler)
        .map(Response::isValid)
        .test()
        .assertComplete()
        .assertNoErrors()
        .assertValue(true);
  }

  @Test
  public void testGetFilteredResponses() {
    RequestHandler handler = Creator.getRequestHandler();
    ParsedRequest request = Creator.createValidRequest();
    Observable.just(request, request).compose(handler)
        .map(Response::getData)
        .map(Optional::get)
        .map(String::new)
        .test()
        .assertComplete()
        .assertNoErrors()
        .assertValues("hello world \r\n", "hello world \r\n");
  }

  @Test
  public void testHandlerInvalidRequest() {
    RequestHandler handler = Creator.getRequestHandler();
    Observable.just(Creator.createInvalidRequest()).compose(handler)
        .map(response -> !response.isValid() && response instanceof ErrorResponse
            && response.getCode() == HttpCode.INTERNAL_SERVER_ERROR)
        .test()
        .assertComplete()
        .assertNoErrors()
        .assertValue(true);
  }
}