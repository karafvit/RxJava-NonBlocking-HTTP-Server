package ass.hamsatom.semestral.response;

import ass.hamsatom.semestral.Creator;
import ass.hamsatom.semestral.constat.HttpCode;
import ass.hamsatom.semestral.constat.HttpMethod;
import ass.hamsatom.semestral.request.ParsedRequest;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * @author Tomáš Hamsa on 12.04.2017.
 */
public class FileResponseTest {

  @Test
  public void testGetMethod() {
    HttpMethod method = HttpMethod.GET;
    ParsedRequest request = Creator.createRequest(Creator.TEST_FILE, "", "", method);
    Response response = new FileResponse(request);
    Assert.assertEquals(response.getMethod(), method);
  }

  @Test
  public void testGetClient() {
    ParsedRequest parsedRequest = Creator.createValidRequest();
    Response response = new FileResponse(parsedRequest);
    Assert.assertNotNull(response.getClient());
  }

  @Test
  public void testGetCodeValid() {
    ParsedRequest parsedRequest = Creator.createValidRequest();
    Response response = new FileResponse(parsedRequest);
    Assert.assertEquals(response.getCode(), HttpCode.OK);
  }

  @Test
  public void testGetCodeInvalid() {
    ParsedRequest parsedRequest = Creator.createInvalidRequest();
    Response response = new FileResponse(parsedRequest);
    Assert.assertEquals(response.getCode(), HttpCode.INTERNAL_SERVER_ERROR);
  }

  @Test
  public void testIsCacheable() {
    FileResponse response = new FileResponse(Creator.createValidRequest());
    Assert.assertTrue(response.isCacheable());
  }

  @Test
  public void testIsNotCacheable() {
    FileResponse response = new FileResponse(Creator.createInvalidRequest());
    Assert.assertFalse(response.isCacheable());
  }

  @Test
  public void testIsValid() {
    FileResponse response = new FileResponse(Creator.createValidRequest());
    Assert.assertTrue(response.isValid());
  }

  @Test
  public void testIsNotValid() {
    FileResponse response = new FileResponse(Creator.createInvalidRequest());
    Assert.assertFalse(response.isValid());
  }


  @Test
  public void testGetData() throws Exception {
    FileResponse response = new FileResponse(Creator.createValidRequest());
    Assert.assertEquals(response.getData().get(), Files.readAllBytes(Paths.get(Creator.TEST_FILE)));
  }

  @Test
  public void testGetDataFromInvalid() {
    FileResponse response = new FileResponse(Creator.createInvalidRequest());
    Assert.assertEquals(response.getData(), Optional.empty());
  }
}