package ass.hamsatom.semestral;

import ass.hamsatom.semestral.constat.Constants;
import ass.hamsatom.semestral.constat.HttpMethod;
import ass.hamsatom.semestral.filter.RequestFilter;
import ass.hamsatom.semestral.filter.RequestFilterImpl;
import ass.hamsatom.semestral.handler.RequestHandler;
import ass.hamsatom.semestral.handler.RequestHandlerImpl;
import ass.hamsatom.semestral.parser.HttpParser;
import ass.hamsatom.semestral.parser.HttpParserImpl;
import ass.hamsatom.semestral.request.ParsedRequest;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.name.Names;
import io.reactivex.Observable;
import java.io.IOException;
import java.nio.channels.SocketChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import javax.annotation.Nonnull;

/**
 * @author Tomáš Hamsa on 12.04.2017.
 */
public abstract class Creator {

  public static final String TEST_FILE = "dataFolder/helloworld.txt";
  public static final String USERNAME = "test";
  public static final String PASSWORD = "123456";
  public static final String PROTECTED_FILE = "dataFolder/protectedFolder/testFile.txt";
  public static final String INDEX_HTML = "index.html";
  public static final String PICTURE = "dataFolder/picture.jpg";
  public static final String HTACCESS = "dataFolder/protectedFolder/.htaccess";
  public static final Path root = Paths.get(".").toAbsolutePath().normalize();
  private static final Injector inject;

  static {
    inject = Guice.createInjector(new AbstractModule() {
      @Override
      protected void configure() {
        bind(String.class).annotatedWith(Names.named(Constants.ROOT_FOLDER_ANNOTATION))
            .toInstance(root.toString());
        bind(RequestHandler.class).to(RequestHandlerImpl.class);
        bind(RequestFilter.class).to(RequestFilterImpl.class);
      }
    });
  }

  @Nonnull
  public static ParsedRequest createInvalidRequest() {
    return createRequest("", "", "", HttpMethod.GET);
  }

  @Nonnull
  public static ParsedRequest createValidRequest() {
    return createRequest(TEST_FILE, "", "", HttpMethod.GET);
  }

  @Nonnull
  public static ParsedRequest createRequest(@Nonnull String key,
      @Nonnull String password,
      @Nonnull String username, @Nonnull HttpMethod method) {
    return new ParsedRequest() {
      @Nonnull
      @Override
      public Path getKey() {
        return Paths.get(key).toAbsolutePath().normalize();
      }

      @Nonnull
      @Override
      public String getBase64dAuthentication() {
        return Base64.getEncoder().encodeToString((username + ":" + password).getBytes());
      }

      @Nonnull
      @Override
      public SocketChannel getClient() {
        try {
          return SocketChannel.open();
        } catch (IOException e) {
          e.printStackTrace();
        }
        return null;
      }

      @Nonnull
      @Override
      public HttpMethod getMethod() {
        return method;
      }
    };
  }

  @Nonnull
  public static RequestFilter getFilter() {
    return inject.getInstance(RequestFilterImpl.class);
  }

  @Nonnull
  public static HttpParser getParser() {
    return inject.getInstance(HttpParserImpl.class);
  }

  @Nonnull
  public static RequestHandler getRequestHandler() {
    return inject.getInstance(RequestHandlerImpl.class);
  }

  @Nonnull
  public static Observable<ParsedRequest> createObservableRequest(@Nonnull String key,
      @Nonnull String password, @Nonnull String username) {
    return Observable.just(createRequest(key, password, username, HttpMethod.GET));
  }
}
