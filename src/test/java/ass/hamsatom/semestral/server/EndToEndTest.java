package ass.hamsatom.semestral.server;

import ass.hamsatom.semestral.Creator;
import ass.hamsatom.semestral.constat.Constants;
import ass.hamsatom.semestral.constat.HttpCode;
import ass.hamsatom.semestral.main.Main;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Base64;
import java.util.Random;
import java.util.StringJoiner;
import javax.annotation.Nonnull;
import lombok.Cleanup;
import org.testng.Assert;
import org.testng.annotations.Test;

public class EndToEndTest {

  private static final String ROOT_LOCATION = ".";


  private String createHttpRequest(@Nonnull String method, @Nonnull String file) {
    return method + " " + file + " HTTP/1.0" + Constants.HTTP_LINE_SEPARATOR
        + Constants.HTTP_LINE_SEPARATOR;
  }

  private String createAuthentication() {
    return "Authorization: Basic " + Base64.getEncoder()
        .encodeToString((Creator.USERNAME + ":" + Creator.PASSWORD).getBytes())
        + Constants.HTTP_LINE_SEPARATOR + Constants.HTTP_LINE_SEPARATOR;
  }

  private byte[] parseReceivedFile(@Nonnull String serverResult) {
    serverResult = serverResult.substring(serverResult.indexOf(Constants.HTTP_LINE_SEPARATOR) + 1);
    return serverResult.getBytes();
  }

  private int getResultCode(@Nonnull String serverResult) {
    String code = serverResult.substring(9, 12);
    return Integer.parseInt(code);
  }

  @Test
  public void testIndex() throws Exception {
    String index = "index.html";
    String requestBody =
        createHttpRequest("GET", index).trim() + Constants.HTTP_LINE_SEPARATOR
            + createAuthentication();
    String serverResult = testServer(requestBody);
    int receivedCode = getResultCode(serverResult);
    Assert.assertEquals(receivedCode, HttpCode.OK.getValue());
  }

  @Test
  public void testWindowsFile() throws Exception {
    String serverResult = testServer(
        createHttpRequest("HEAD", "C:/windows/system32/MessagingService.dll"));
    int receivedCode = getResultCode(serverResult);
    Assert.assertEquals(receivedCode, HttpCode.NOT_FOUND.getValue());
  }

  @Test
  public void testPicture() throws Exception {
    String requestBody =
        createHttpRequest("GET", Creator.PICTURE).trim() + Constants.HTTP_LINE_SEPARATOR
            + createAuthentication();
    String serverResult = testServer(requestBody);
    int receivedCode = getResultCode(serverResult);
    Assert.assertEquals(receivedCode, HttpCode.OK.getValue());
  }

  @Test
  public void testHtaccess() throws Exception {
    String serverResult = testServer(createHttpRequest("HEAD", Creator.HTACCESS));
    int receivedCode = getResultCode(serverResult);
    Assert.assertEquals(receivedCode, HttpCode.NOT_FOUND.getValue());
  }

  @Test
  public void testInvalidMethod() throws Exception {
    String serverResult = testServer(
        createHttpRequest("NONEXISTING_METHOD", Creator.PICTURE));
    int receivedCode = getResultCode(serverResult);
    Assert.assertEquals(receivedCode, HttpCode.BAD_REQUEST.getValue());
  }


  private String testServer(@Nonnull String input) throws IOException, InterruptedException {
    int port = new Random().nextInt(2 * Short.MAX_VALUE);

    // run server
    new Thread(() -> {
      try {
        Main.main(new String[]{String.valueOf(port), ROOT_LOCATION});
      } catch (Exception e) {
        e.printStackTrace();
      }
    }).start();

    // wait for server to start
    Thread.sleep(1000);

    @Cleanup Socket sock = new Socket("localhost", port);
    @Cleanup BufferedReader br = new BufferedReader(
        new InputStreamReader(new BufferedInputStream(sock.getInputStream())));
    sock.getOutputStream().write(input.getBytes());

    StringJoiner message = new StringJoiner(Constants.HTTP_LINE_SEPARATOR);
    String curLine;
    while ((curLine = br.readLine()) != null) {
      message.add(curLine);
    }

    // Append EOL because BufferedReader.readLine() discards EOL
    return message.toString();
  }
}