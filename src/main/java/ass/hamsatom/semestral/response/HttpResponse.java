package ass.hamsatom.semestral.response;

import java.nio.channels.SocketChannel;
import javax.annotation.Nonnull;

/**
 * Represents response that will be send via http to client's {@link SocketChannel}
 *
 * @author Tomáš Hamsa on 16.05.2017.
 */
public interface HttpResponse {

  @Nonnull
  SocketChannel getClient();

  @Nonnull
  byte[] getData();

}
