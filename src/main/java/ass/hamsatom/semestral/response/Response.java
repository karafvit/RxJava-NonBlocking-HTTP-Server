package ass.hamsatom.semestral.response;

import ass.hamsatom.semestral.constat.HttpCode;
import ass.hamsatom.semestral.constat.HttpMethod;
import java.nio.channels.SocketChannel;
import java.util.Optional;
import javax.annotation.Nonnull;

/**
 * Represents a response to {@link ass.hamsatom.semestral.request.ParsedRequest}
 */
public interface Response {

  /**
   * @return true if this response can be cached. One might want to return false for all error
   * response so that they are not cached
   */
  boolean isCacheable();

  /**
   * Indicates whenever the response has valid data or not
   *
   * @return true if data are perfectly valid, false if they aren't
   */
  boolean isValid();

  /**
   * Provides code of response
   *
   * @return Response code according to HTTP standard
   */
  HttpCode getCode();

  /**
   * Provides data
   *
   * @return Optional with data that origin request demanded or Optional.empty if the data couldn't
   * be read
   */
  @Nonnull
  Optional<byte[]> getData();

  /**
   * Provides channel from which the request arrived
   *
   * @return channel on which the response should be written
   */
  @Nonnull
  SocketChannel getClient();

  /**
   * Provides http method on which this response responds
   *
   * @return http method
   */
  @Nonnull
  HttpMethod getMethod();
}
