package ass.hamsatom.semestral.response;

import ass.hamsatom.semestral.request.ParsedRequest;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Response that gets it's data from File specified in key of {@link ParsedRequest} which was used
 * to create instance of this class.
 */
public class FileResponse extends DataResponse {

  private static final Logger LOG = LoggerFactory.getLogger(FileResponse.class);

  /**
   * Creates new response that gets it'S data from file
   *
   * @param originParsedRequest Request to which this response responds
   */
  public FileResponse(@Nonnull ParsedRequest originParsedRequest) {
    super(originParsedRequest.getMethod(), originParsedRequest.getClient());
    byte[] data = processData(originParsedRequest.getKey().toString());
    setData(data);
  }

  /**
   * Read data from file
   *
   * @param filePath file from which the data will be read
   */
  @Nullable
  private byte[] processData(@Nonnull String filePath) {
    byte[] data = null;
    //Try-with-resource in order to close FileChannel even if exception happened
    try (FileChannel fileChannel = new RandomAccessFile(filePath, "r").getChannel()) {
      MappedByteBuffer buffer = fileChannel
          .map(FileChannel.MapMode.READ_ONLY, 0, fileChannel.size());
      data = new byte[buffer.limit()];
      for (int i = 0; i < data.length; ++i) {
        data[i] = buffer.get();
      }
    } catch (IOException e) {
      LOG.warn("Error while creating response from file" + filePath, e);
      //no need to change dataValid to false because they are never set to true
    }
    return data;
  }

  @Override
  public boolean isCacheable() {
    return isValid();
  }
}
