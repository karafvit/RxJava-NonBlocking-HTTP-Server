package ass.hamsatom.semestral.response;

import ass.hamsatom.semestral.request.ParsedRequest;
import ass.hamsatom.semestral.util.Cache;
import javax.annotation.Nonnull;

/**
 * Response that gets it's data from {@link Cache}
 *
 * @author Tomáš Hamsa on 02.04.2017.
 */
public class CacheResponse extends DataResponse {

  /**
   * Absolute path to file with filename
   */
  private final String filePath;

  /**
   * Creates new response that gets it'S data from {@link Cache}
   *
   * @param originParsedRequest Request to which this response responds
   */
  public CacheResponse(@Nonnull ParsedRequest originParsedRequest) {
    super(originParsedRequest.getMethod(), originParsedRequest.getClient());
    filePath = originParsedRequest.getKey().toString();
    setData(obtainData());
  }

  /**
   * Read data from {@link Cache} and set validity to true if data were successfully read.
   */
  private byte[] obtainData() {
    return Cache.getData(filePath).orElse(null);
  }


  @Override
  public boolean isCacheable() {
    return isValid() && !Cache.containsValidData(filePath);
  }


}
