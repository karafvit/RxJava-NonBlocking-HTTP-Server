package ass.hamsatom.semestral.filter;

import ass.hamsatom.semestral.constat.Constants;
import ass.hamsatom.semestral.constat.HttpCode;
import ass.hamsatom.semestral.handler.RequestHandler;
import ass.hamsatom.semestral.request.ParsedRequest;
import ass.hamsatom.semestral.response.ErrorResponse;
import ass.hamsatom.semestral.response.Response;
import ass.hamsatom.semestral.util.AccessAuthentication;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.annotation.Nonnull;


/**
 * Filters request so that only requests that demand valid, readable data are processed. If the
 * request demands access to protected folder the request is processed only if the request has
 * correct username and password.
 *
 * @author Tomáš Hamsa on 07.04.2017.
 */
public class RequestFilterImpl implements RequestFilter {

  /**
   * Handler used to process valid requests
   */
  private RequestHandler requestHandler;
  /**
   * Folder highest int the filesystem hierarchy to which users have access.
   */
  private Path rootFolder;

  /**
   * Filters request so that only requests that demand valid, readable data are processed. If the
   * request demands access to protected folder the request is processed only if the request has
   * correct username and password.
   *
   * @param rootFolder The root folder of the server. All used files are under this location
   * @param requestHandler Handler that maps requests to responses
   */
  @Inject
  public RequestFilterImpl(
      @Nonnull @Named(Constants.ROOT_FOLDER_ANNOTATION) String rootFolder,
      @Nonnull RequestHandler requestHandler) {
    this.rootFolder = Paths.get(rootFolder).toAbsolutePath().normalize();
    this.requestHandler = requestHandler;
  }

  /**
   * Determines whenever requests demands valid data that are under root folder
   *
   * @param parsedRequest parsedRequest that will be checked
   * @return {@code true} if the data demanded by parsedRequest are readable and it's a file and
   * it's location is under {@code rootFolder} of this handler
   */
  private boolean isValidRequest(@Nonnull ParsedRequest parsedRequest) {
    Path filePath = parsedRequest.getKey();
    File file = filePath.toFile();
    return file.canRead() && file.isFile() && filePath.startsWith(rootFolder) && !filePath
        .endsWith(Constants.AUTHORIZATION_FILE);
  }


  /**
   * Gets response for each provided request
   *
   * @param requests Observable of requests that this method will map to response
   * @return Observable of response.
   */
  @Override
  @Nonnull
  public ObservableSource<Response> apply(@Nonnull Observable<ParsedRequest> requests) {
        /*
        This is the only efficient solution to split Observables according to some condition. The
         only different solution would be to create multiple
        Observables from filtered original observable. However with handler you need to compare
        each member of the stream so to create 3 filtered Observable
        you need to compare each member three time resulting in O(3n) complexity. This solution
        compares each value only once so it runs in O(n).
         */
    return requests.groupBy(request -> {
      if (!isValidRequest(request)) {
        return RequestValidity.INVALID;
      } else if (!AccessAuthentication.hasAccess(rootFolder, request)) {
        return RequestValidity.INSUFFICIENT_RIGHTS;
      } else {
        return RequestValidity.VALID;
      }
    }).flatMap(labeledRequest -> {//Map request to appropriate response according to validity
      switch (labeledRequest.getKey()) {
        case VALID:
          return labeledRequest.compose(requestHandler);
        case INVALID:
          return labeledRequest.map(request -> new ErrorResponse(HttpCode.NOT_FOUND, request));
        case INSUFFICIENT_RIGHTS:
          return labeledRequest.map(request -> new ErrorResponse(HttpCode.UNAUTHORIZED, request));
        default:
          return labeledRequest
              .map(request -> new ErrorResponse(HttpCode.INTERNAL_SERVER_ERROR, request));
      }
    });
  }

  private enum RequestValidity {
    /**
     * Means that the request demand valid, readable file that has access to folder
     */
    VALID,
    /**
     * Means that the request demand to read file that is not readable or is not under root folder
     */
    INVALID,
    /**
     * Means that the request doesn't have access to protected folder because it'S password and
     * username don't match the user that have access to that folder
     */
    INSUFFICIENT_RIGHTS
  }
}
