package ass.hamsatom.semestral.server;

import ass.hamsatom.semestral.parser.HttpParser;
import ass.hamsatom.semestral.writer.ResponseBuilder;
import java.io.IOException;
import javax.annotation.Nonnull;

/**
 * Http server that process requests and maps the to appropriate http response
 */
public interface Server {

  /**
   * Runs server with given handler
   *
   * @param handler - ObservableTransformer that processes requests
   * @param writer - Consumer that consumes request and writes it to client
   */
  void run(@Nonnull HttpParser parser, @Nonnull ResponseBuilder writer)
      throws IOException;

  /**
   * Stops the server and all components used in it like Response writter
   */
  void stop();
}
