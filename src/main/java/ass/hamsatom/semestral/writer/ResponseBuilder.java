package ass.hamsatom.semestral.writer;

import ass.hamsatom.semestral.response.Response;
import io.reactivex.functions.Consumer;

/**
 * Creates HTTP responses from data in Response and adds them to writing queue
 *
 * @author Tomáš Hamsa on 11.05.2017.
 */
public interface ResponseBuilder extends Consumer<Response> {

  /**
   * Stops the writing in given {@link WritingQueue}
   */
  void endWriting();
}
