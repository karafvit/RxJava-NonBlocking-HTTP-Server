package ass.hamsatom.semestral.util;

import java.lang.ref.SoftReference;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;
import javax.annotation.Nonnull;

/**
 * Cache with data stored inside it. The data are stored in byte array using {@link SoftReference}
 * so if the JVM needs more space than the reference are dropped and object deleted. That should
 * prevent OutOfMemoryException.
 *
 * @author Tomáš Hamsa on 02.04.2017.
 */
public class Cache {

  /**
   * Cache with data from files
   */
  private static final Map<String, SoftReference<byte[]>> REFERENCE_CACHE = new ConcurrentHashMap<>();

  /**
   * As this is utility class with static methods and field this class should not be ever
   * initialized
   *
   * @throws IllegalAccessException if someone tries to initialize this class
   */
  private Cache() throws IllegalAccessException {
    throw new IllegalAccessException("Initializing utility class");
  }

  /**
   * Indicates whenever the cache contains valid data for given filePath. The data are considered to
   * be valid when they are present in cache and also the reference to the data is not enqued so the
   * JVM doesn't plan to delete the object. This method is thread-safe without synchronization so it
   * can be called from multiple instances at the same time without blocking.
   *
   * @param filePath absolute path of file with name of the file. It's used as a key in cache so all
   * data that the file contains are assigned to this given key.
   * @return true only if cache contains data for given key and the data are not enqued, false
   * otherwise
   */
  public static boolean containsValidData(@Nonnull String filePath) {
    SoftReference<byte[]> softReference = REFERENCE_CACHE.get(filePath);
    return softReference != null && !softReference.isEnqueued();
  }

  /**
   * Provides data from cache. This method is exception-safe and doesn't throw any RuntimeException.
   * In case data is not present in cache or exception happened in the process of obtaining data
   * than this class returns {@code Observable.empty ()} but doesn't throw any exception. This
   * method is thread-safe without synchronization so it can be accessed from multiple instances at
   * once without blocking.
   *
   * @param filePath absolute path of file with name of the file. It's used as a key in cache so all
   * data that the file contains are assigned to this given key.
   * @return Observable of data. Observable obtains data if the data are present in cache and are
   * not enqued to be deleted. Otherwise returns empty Observable
   */
  @Nonnull
  public static Optional<byte[]> getData(@Nonnull String filePath) {
    return Stream.of(REFERENCE_CACHE.get(filePath)).filter(Objects::nonNull)
        .filter(reference -> !reference.isEnqueued()).map(SoftReference::get).findAny();
  }

  /**
   * Saves provided data to cache
   *
   * @param filePath absolute path of the file used as a key in cache
   * @param data data that the file contains, used as a value in cache
   */
  public static void cacheInMemory(@Nonnull String filePath, @Nonnull byte[] data) {
    REFERENCE_CACHE.put(filePath, new SoftReference<>(data));
  }
}
