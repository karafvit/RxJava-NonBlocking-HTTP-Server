package ass.hamsatom.semestral.util;

import ass.hamsatom.semestral.constat.Constants;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import javax.annotation.Nonnull;

/**
 * Utility class for operation with {@link ByteBuffer}
 */
public class ByteBufferUtil {

  /**
   * As this is utility class with static methods and field this class should not be ever
   * initialized
   *
   * @throws IllegalAccessException if someone tries to initialize this class
   */
  private ByteBufferUtil() throws IllegalAccessException {
    throw new IllegalAccessException("Initializing utility class with only static methods");
  }

  /**
   * Provides reading from {@link ByteBuffer} and {@link SocketChannel}. Reads part of the message
   * and returns it as ASCII String. Length of the part is determinate by the size of provided
   * ByteBuffer.
   *
   * @param buffer prealocated buffer with suitable size; allocate with ByteBuffer.allocate() or
   * ByteBuffer.allocateDirect()
   * @param channel - readable channel
   * @return ASCII String
   */
  @Nonnull
  public static String readFromChannel(@Nonnull ByteBuffer buffer,
      @Nonnull SocketChannel channel) throws IOException {
    int limit = buffer.limit();

    channel.read(buffer);
    buffer.flip();

    byte[] bytes = new byte[buffer.limit()];
    buffer.get(bytes, 0, buffer.limit());

    // reset buffer
    buffer.position(0);
    buffer.limit(limit);

    // telnet sends ASCII
    return new String(bytes, Constants.TELNET_CHARSET);
  }
}