package ass.hamsatom.semestral.parser;

import ass.hamsatom.semestral.constat.Constants;
import ass.hamsatom.semestral.constat.HttpCode;
import ass.hamsatom.semestral.constat.HttpMethod;
import ass.hamsatom.semestral.filter.RequestFilter;
import ass.hamsatom.semestral.request.HttpRequest;
import ass.hamsatom.semestral.request.ParsedRequest;
import ass.hamsatom.semestral.request.ParsedRequestImpl;
import ass.hamsatom.semestral.response.ErrorResponse;
import ass.hamsatom.semestral.response.Response;
import ass.hamsatom.semestral.server.Server;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import java.io.File;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.StringTokenizer;
import javax.annotation.Nonnull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Parses raw {@link HttpRequest}s received in {@link Server} to {@link ParsedRequest} and then maps
 * them to appropriate {@link Response}
 *
 * @author Tomáš Hamsa on 11.05.2017.
 */
public class HttpParserImpl implements HttpParser {

  private static final Logger LOG = LoggerFactory.getLogger(HttpParserImpl.class);
  /**
   * Name of POST method in HTTP head
   */
  private static final String POST_METHOD = "POST";

  /**
   * Name of HEAD method in HTTP head
   */
  private static final String HEAD_METHOD = "HEAD";

  /**
   * Name of GET method in HTTP head
   */
  private static final String GET_METHOD = "GET";

  /**
   * Prefix for line in HTTP that contains authentication data
   */
  private static final String AUTHORIZATION_PREFIX = "Authorization:";
  /**
   * Authentication type
   */
  private static final String AUTHORIZATION_LABEL = "Basic";

  /**
   * File provided instead of root
   */
  private static final String ROOT_DIRECTORY = "index.html";
  /**
   * Symbol used to demand root folder
   */
  private static final String ROOT_SYMBOL = "/";
  /**
   * Symbol that separates words in http request
   */
  private static final String WORDS_SEPARATOR = " ";

  /**
   * Filter used to filter invalid requests and map them to appropriate response
   */
  private final RequestFilter filter;

  /**
   * Root folder of the server. All valid http request points to files under this root
   */
  private final String rootPath;

  /**
   * Parses raw {@link HttpRequest}s received in {@link Server} to {@link ParsedRequest} and then
   * maps them to appropriate {@link Response}
   *
   * @param filter Reuqest filter that will be used to map request to invalid location or without
   * authorization
   * @param rootPath Path to the root folder of the server. This is the location from which the
   * files will be served
   */
  @Inject
  public HttpParserImpl(@Nonnull RequestFilter filter,
      @Nonnull @Named(Constants.ROOT_FOLDER_ANNOTATION) String rootPath) {
    this.filter = filter;
    this.rootPath = rootPath;
  }

  /**
   * Parses request from {@link HttpRequest} that constains raw data of it's body
   *
   * @param request Request received in {@link Server}
   * @return Request with parsed data
   */
  private static ParsedRequest parseRequest(@Nonnull HttpRequest request,
      @Nonnull String rootFolder) {
    // Default string for user authentication
    String authorization = "";
    //Default path
    Path nioPath = Paths.get(rootFolder).toAbsolutePath().normalize();
    HttpMethod method;
    try {
      // Replaced all EOL with space to make parsing easier
      String requestBody = request.getRequestBody()
          .replaceAll(Constants.HTTP_LINE_SEPARATOR, WORDS_SEPARATOR);
      // Splits string according to delimiter
      StringTokenizer tokens = new StringTokenizer(requestBody, WORDS_SEPARATOR);
      // First word in HTTP request is name of method
      method = getMethodFromName(tokens.nextToken());
      // Second word is path to file
      String path = tokens.nextToken();
      // Decode path from URL, the decoder doesn't support %20 whitespaces
      path = new URI(path).getPath().replaceAll("%20", " ");
      // If path leads to root, map it to index file
      path = ROOT_SYMBOL.equals(path) ? ROOT_DIRECTORY : path;
      // Map string path to java path
      nioPath = new File(rootFolder, path).toPath().toAbsolutePath().normalize();
      // Third word is http version which we don't need
      tokens.nextToken();
      // Look until authentication is found or end of request
      while (tokens.hasMoreTokens()) {
        if (AUTHORIZATION_PREFIX.equals(tokens.nextToken()) && AUTHORIZATION_LABEL
            .equals(tokens.nextToken())) {
          authorization = tokens.nextToken();
          break;
        }
      }
    } catch (Exception e) {
      LOG.info("Error while parsing http request", e);
      // If exception happened while parsing then the request must have been invalid
      method = HttpMethod.NOT_SUPPORTED;
    }
    LOG.debug(nioPath.toString());
    return new ParsedRequestImpl(request.getChannel(), nioPath, authorization, method);
  }

  /**
   * Return Enum representation of request method
   *
   * @param name String name of method as present in http request
   * @return enum representation of name
   */
  @Nonnull
  private static HttpMethod getMethodFromName(@Nonnull String name) {
    switch (name) {
      case POST_METHOD:
        return HttpMethod.POST;
      case GET_METHOD:
        return HttpMethod.GET;
      case HEAD_METHOD:
        return HttpMethod.HEAD;
      default:
        return HttpMethod.NOT_SUPPORTED;
    }
  }

  /**
   * Parse all http request from {@link Server} and map them to appropriate {@link Response}
   *
   * @param requests Requests received in {@link Server} that will be mapped to {@link Response}
   * @return Appropriate responses
   */
  @Override
  @Nonnull
  public ObservableSource<Response> apply(@Nonnull Observable<HttpRequest> requests) {
    return requests.map(request1 -> parseRequest(request1, rootPath))
        .groupBy(ParsedRequest::getMethod)
        .flatMap(labeledRequest -> {
          switch (labeledRequest.getKey()) {
            case GET:
            case HEAD:
              return labeledRequest.compose(filter);
            case POST:
              return labeledRequest
                  .map(request -> new ErrorResponse(HttpCode.NOT_IMPLEMENTED, request));
            default:
              return labeledRequest
                  .map(request -> new ErrorResponse(HttpCode.BAD_REQUEST, request));
          }
        });
  }
}
