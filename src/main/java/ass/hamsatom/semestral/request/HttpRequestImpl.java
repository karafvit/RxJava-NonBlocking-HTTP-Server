package ass.hamsatom.semestral.request;

import java.nio.channels.SocketChannel;
import javax.annotation.Nonnull;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Tomáš Hamsa on 11.05.2017.
 */
@AllArgsConstructor
public class HttpRequestImpl implements HttpRequest {

  @Getter
  @Nonnull
  private final SocketChannel channel;

  @Getter
  @Nonnull
  private final String requestBody;
}
