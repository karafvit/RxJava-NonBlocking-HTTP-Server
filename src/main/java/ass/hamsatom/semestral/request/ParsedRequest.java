package ass.hamsatom.semestral.request;

import ass.hamsatom.semestral.constat.HttpMethod;
import java.nio.channels.SocketChannel;
import java.nio.file.Path;
import javax.annotation.Nonnull;

/**
 * Represents a request for an arbitrary value from an arbitrary provider and holds key, that
 * corresponds to a single value. Key might be a normalized FileSystem path, database index key,
 * etc.
 */
public interface ParsedRequest {

  /**
   * Returns absolute path to the file this request demands
   */
  @Nonnull
  Path getKey();

  /**
   * Provides Base64 encoded string with authentication data
   *
   * @return Base64 encoded string in format username:password
   */
  @Nonnull
  String getBase64dAuthentication();


  /**
   * Provides channel from which the request arrived
   *
   * @return channel of origin
   */
  @Nonnull
  SocketChannel getClient();

  /**
   * Provides http method associated with this request
   *
   * @return http method
   */
  @Nonnull
  HttpMethod getMethod();
}
