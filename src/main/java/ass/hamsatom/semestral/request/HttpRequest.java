package ass.hamsatom.semestral.request;

import java.nio.channels.SocketChannel;
import javax.annotation.Nonnull;

/**
 * Represents raw http request received in {@link Server}
 *
 * @author Tomáš Hamsa on 11.05.2017.
 */
public interface HttpRequest {

  /**
   * Provides channel from which the request arrived
   *
   * @return channel of origin
   */
  @Nonnull
  SocketChannel getChannel();

  /**
   * Provides the contain of http request
   *
   * @return Text present in HTTP request
   */
  @Nonnull
  String getRequestBody();
}
